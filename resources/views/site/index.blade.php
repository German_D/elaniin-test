@extends('layouts.base')
@section('content')
    <div class="container p-4">
        <div class="row justify-content-center">
            @foreach($products as $product)
                <div class="col-md-3">
                    <div class="card" style="width: 18rem;">
                        <img src="{{asset('storage/images')}}/{{$product->image}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{$product->product_name}}</h5>
                        </div>
                        <div class="card-body">
                            <a href="{{url('/product', $product->id)}}" class="card-link">View</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop
