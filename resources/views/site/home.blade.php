@extends('layouts.base')

@section('content')
<div class="container p-4">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Products</h5>
                </div>
                <div class="card-body">
                    <a href="{{url('/product/manage')}}" class="card-link">Manage</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Users</h5>
                </div>
                <div class="card-body">
                    <a href="{{url('/users/manage')}}" class="card-link">Manage</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
