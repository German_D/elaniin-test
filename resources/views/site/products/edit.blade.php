@extends('layouts.base')
@section('content')
    <div class="container p-4">
        <div class="text-center">
            <h3>Editing {{$product->product_name}}</h3>
        </div>
        <br>

        <form class="row g-3" action="{{route('product.update', $product->id)}}" method="POST" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">SKU</label>
                <input type="text" name="sku" class="form-control" id="inputEmail4" value="{{$product->sku}}">
            </div>
            <div class="col-md-6">
                <label for="inputPassword4" class="form-label">Product Name</label>
                <input type="text" name="product_name" class="form-control" id="inputPassword4" value="{{$product->product_name}}">
            </div>
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">Quantity</label>
                <input type="number" name="quantity" class="form-control" id="inputEmail4" value="{{$product->quantity}}">
            </div>
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">Price</label>
                <input type="number" name="price" class="form-control" id="inputEmail4" value="{{$product->price}}">
            </div>
            <div class="col-md-12">
                <label for="inputPassword4" class="form-label">Description</label>
                <textarea  name="description" class="form-control" id="inputPassword4" rows="5">{{$product->description}}</textarea>
            </div>
            <div class="col-md-12">
                <label for="inputPassword4" class="form-label">Image</label>
                <input type="file" name="image" class="form-control" id="inputPassword4">
            </div>

            <div class="col-12">
                <a href="{{url('/product/manage')}}" class="btn btn-outline-secondary">Cancel</a>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div>
@stop
