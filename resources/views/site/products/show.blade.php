@extends('layouts.base')
@section('content')
    <div class="container p-4">
        @guest()
            <a href="{{url('/')}}" class="btn btn-primary">Return To Products</a>
        @else
            <a href="{{url('/product/manage')}}" class="btn btn-primary">Return To Products</a>
        @endguest

        <div class="row justify-content-center">
            <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                    <img src="{{asset('storage/images')}}/{{$product->image}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">{{$product->product_name}}</h5>
                        <h5 class="card-title">Stock {{$product->quantity}}</h5>
                        <h5 class="card-title">Price ${{$product->price}}</h5>
                        <p class="card-title">{{$product->description}}</p>
                    </div>
                    @auth()
                    <div class="card-body">
                        <a href="{{url('/product',$product->id)}}/edit" class="card-link">Edit</a>
                        <form action="{{route('product.destroy', $product->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="card-link">Delete</button>
                        </form>
                    </div>
                    @endauth
                </div>
            </div>
        </div>
    </div>
@stop
