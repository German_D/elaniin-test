@extends('layouts.base')
@section('content')
    <div class="container p-4">
        <div class="text-center">
            <h3>Creating New Product</h3>
        </div>
        <br>

        <form class="row g-3" action="{{route('product.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">SKU</label>
                <input type="text" name="sku" class="form-control" id="inputEmail4">
            </div>
            <div class="col-md-6">
                <label for="inputPassword4" class="form-label">Product Name</label>
                <input type="text" name="product_name" class="form-control" id="inputPassword4">
            </div>
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">Quantity</label>
                <input type="number" name="quantity" class="form-control" id="inputEmail4">
            </div>
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">Price</label>
                <input type="number" name="price" class="form-control" id="inputEmail4">
            </div>
            <div class="col-md-12">
                <label for="inputPassword4" class="form-label">Description</label>
                <textarea  name="description" class="form-control" id="inputPassword4" rows="5"></textarea>
            </div>
            <div class="col-md-12">
                <label for="inputPassword4" class="form-label">Image</label>
                <input type="file" name="image" class="form-control" id="inputPassword4">
            </div>

            <div class="col-12">
                <a href="{{url('/product/manage')}}" class="btn btn-outline-secondary">Cancel</a>
                <button type="submit" class="btn btn-primary">Register</button>
            </div>
        </form>
    </div>
@stop
