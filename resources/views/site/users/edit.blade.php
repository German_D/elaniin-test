@extends('layouts.base')
@section('content')
    <div class="p-4">
        <div class="text-center">
            <h3>Update User</h3>
            <br>
        </div>

        <form class="row g-3" action="{{route('user.update', $user->id)}}" method="POST">
            @method('PUT')
            @csrf
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">Name</label>
                <input type="text" name="name" class="form-control" id="inputEmail4" value="{{$user->name}}">
            </div>
            <div class="col-md-6">
                <label for="inputPassword4" class="form-label">Username</label>
                <input type="text" name="username" class="form-control" id="inputPassword4" value="{{$user->username}}">
            </div>
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">Email</label>
                <input type="email" name="email" class="form-control" id="inputEmail4" value="{{$user->email}}">
            </div>
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">Phone</label>
                <input type="tel" name="phone" class="form-control" id="inputEmail4" value="{{$user->phone}}">
            </div>
            <div class="col-md-12">
                <label for="inputPassword4" class="form-label">Date of Birth</label>
                <input type="date" name="dateOfBirth" class="form-control" id="inputPassword4" value="{{$user->dateOfBirth}}">
            </div>
            <div class="col-md-12">
                <label for="inputPassword4" class="form-label">Password</label>
                <input type="password" name="password" class="form-control" id="inputPassword4" value="{{$user->password}}">
            </div>

            <div class="col-12">
                <a href="{{url('/user/manage')}}" class="btn btn-outline-secondary">Cancel</a>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div>
@stop
