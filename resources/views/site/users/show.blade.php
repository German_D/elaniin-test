@extends('layouts.base')
@section('content')
    <div class="container p-4">

        <a href="{{url('/user/manage')}}" class="btn btn-primary">Return To Users</a>

        <div class="row justify-content-center">
            <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h3 class="card-title">{{$user->name}}</h3>
                        <h5 class="card-title">Stock {{$user->username}}</h5>
                        <h5 class="card-title">Price ${{$user->phone}}</h5>
                        <p class="card-title">{{$user->email}}</p>
                    </div>
                    @auth()
                        <div class="card-body">
                            <a href="{{url('/user',$user->id)}}/edit" class="card-link">Edit</a>
                            <form action="{{route('user.destroy', $user->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="card-link">Delete</button>
                            </form>
                        </div>
                    @endauth
                </div>
            </div>
        </div>
    </div>
@stop
