@extends('layouts.base')
@section('content')
    <div class="container p-4">
        <a href="#" class="btn btn-primary">Create User</a>
        <div class="row justify-content-center">
            @foreach($users as $user)
                <div class="col-md-3">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">{{$user->name}}</h5>
                            <small>{{$user->username}}</small>
                        </div>
                        <div class="card-body">
                            <a href="{{url('/user', $user->id)}}" class="card-link">View</a>
                            <a href="{{url('/user',$user->id)}}/edit" class="card-link">Edit</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop
