<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MainController extends Controller
{
    protected $user;
    protected $product;

    public function __construct(User $user, Product $product)
    {
        $this->user = $user;
        $this->product = $product;
    }

    public function index() {
        $products = $this->product->paginate(8);

        return view('site.index', compact('products'));
    }

    // User Functions
    public function manageUsers() {
        $users = $this->user->paginate(8);

        return view('site.users.manage', compact('users'));
    }

    public function showUser($id) {
        $user = $this->user->find($id);

        return view('site.users.show', compact('user'));
    }

    public function editUser($id) {
        $user = $this->user->find($id);

        return view('site.users.edit', compact('user'));
    }

    public function updateUser(Request $request, $id) {
        $user = $this->user->find($id);

        $user->update($request->all());

        return redirect()->to('/user/manage');
    }

    public function destroyUser($id) {
        $this->user->destroy($id);

        return redirect()->to('/user/manage');
    }
    //End User functions

    // Product functions

    public function manageProducts() {
        $products = $this->product->paginate(8);

        return view('site.products.manage', compact('products'));
    }

    public function createProduct() {
        return view('site.products.create');
    }

    public function storeProduct(Request $request) {
        $validRequest = $request->validate([
            'product_name' => 'required|string',
            'quantity' => 'required|integer',
            'price' => 'required'
        ]);

        if ($validRequest) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $image->move(storage_path() . '/app/public/images', $name);

            $product = new Product;
            $product->sku = $request->sku;
            $product->product_name = $request->product_name;
            $product->quantity = $request->quantity;
            $product->price = $request->price;
            $product->description = $request->description;
            $product->image = $name;
            $product->save();

            return redirect()->to('/product/manage');
        } else {
            return redirect()->back()->with('error', $validRequest);
        }
    }

    public function showProduct($id) {
        $product = $this->product->find($id);

        return view('site.products.show', compact('product'));
    }

    public function editProduct($id) {
        $product = $this->product->find($id);

        return view('site.products.edit', compact('product'));
    }

    public function updateProduct(Request $request, $id) {
        $product = $this->product->find($id);

        $product->update($request->all());

        return redirect()->to('/product/manage');
    }

    public function destroyProduct($id) {
        $this->product->destroy($id);

        return redirect()->to('/product/manage');
    }

    // End product functions
}
