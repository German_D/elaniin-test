<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = ['sku', 'product_name', 'quantity', 'price', 'description', 'image'];

    protected $casts = [
        'quantity' => 'integer'
    ];
}
