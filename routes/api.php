<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UsersAPIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {
    // products endpoints
    Route::group([
        'middleware' => 'api'
    ], function ($router) {
        Route::apiResource('products', 'App\Http\Controllers\ProductController');
    });
    // end product endpoints

    // users endpoints
    Route::post('/user/register', [UsersAPIController::class, 'store']);

    Route::group([
       'middleware' => 'api',
       'prefix' => 'auth'
    ], function ($router) {
        Route::post('/login', [UsersAPIController::class, 'login']);
        Route::post('/logout', [UsersAPIController::class, 'logout']);
        Route::post('/refresh', [UsersAPIController::class, 'refresh']);
        Route::get('/me', [UsersAPIController::class, 'me']);
    });

    Route::group([
        'middleware' => 'api'
    ], function ($router) {
       Route::apiResource('users', 'App\Http\Controllers\UsersAPIController');
    });
    // end users endpoints
});

