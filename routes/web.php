<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Product Routes
Route::group(['middleware' => 'auth'], function () {
    Route::get('/product/manage', [MainController::class, 'manageProducts']);
    Route::get('/product/create', [MainController::class, 'createProduct']);
    Route::post('/product/store', [MainController::class, 'storeProduct'])->name('product.store');
    Route::get('/product/{id}/edit', [MainController::class, 'editProduct']);
    Route::put('/product/update/{id}', [MainController::class, 'updateProduct'])->name('product.update');
    Route::delete('/product/delete/{id}', [MainController::class, 'destroyProduct'])->name('product.destroy');
});
Route::get('/product/{id}', [MainController::class, 'showProduct']);

// End Product Routes

// User Routes
Route::group(['middleware' => 'auth'], function () {
    Route::get('/user/manage', [MainController::class, 'manageUsers']);
    Route::get('/user/{id}', [MainController::class, 'showUser']);
    Route::get('/user/{id}/edit', [MainController::class, 'editUser']);
    Route::put('/user/update/{id}', [MainController::class, 'updateUser'])->name('user.update');
    Route::delete('/user/delete/{id}', [MainController::class, 'destroyUser'])->name('user.destroy');
});
// End user routes
