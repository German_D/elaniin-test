const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copyDirectory('resources/fonts', 'public/css')
    .copyDirectory('node_modules/font-awesome/fonts', 'public/fonts')
    .js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.sass', '../resources/css')
    .options({ processCssUrls: false })
    .styles('resources/css/app.css', 'public/css/app.css')
    .copyDirectory('resources/images', 'public/images')
    .copyDirectory('resources/videos', 'public/videos')
    .browserSync('127.0.0.1:8000');
