
## Elaniin Test

Steps to run this application in your local environment

- copy this repository
- after copied the repo, you need copy the content in the `.env.example` file into new file named `.env`
- run `composer install` to install all the dependencies and create the vendor folder
- After the installation, you can run `php artisan key:generate` to set an application key for the project
- You need to set up your database connection, feel free to use your credentials in the `DB_CONNECTION` section, in this case we are using MySql
- Run the migrations with the command `php artisan migrate`
- Run `php artisan serve`, and you will be ready to test the project.


Thanks guys for the opportunity.

